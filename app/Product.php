<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Product extends Model
{
    protected $fillable = [
        'title' , 'description' , 'price' , 'inventory' , 'view_count','image'
    ];

//    public function toSerachableArray()
//    {
//        $array = $this->toArray();
//
//        if ($this->categories()) {
//            $array['categories'] = $this->categories()->pluck('name');
//        }
//
//        return $array;
////        return Arr::only($array , ['title','description','categories']);
//    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class ,'commentable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class)->using(ProducrAttributeValues::class)->withPivot(['value_id']);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class);
    }
}
