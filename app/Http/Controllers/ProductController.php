<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->paginate(12);
        return view('home.products',compact('products'));
    }

    public function single(Product $product)
    {
        $view =  $product->view_count;
        $product->update([
            'view_count' => $view + 1
        ]);

        return view('home.single-product' , compact('product'));
    }
}
