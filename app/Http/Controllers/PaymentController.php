<?php

namespace App\Http\Controllers;

use App\Helpers\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    public function payment()
    {
        $cartItems = Cart::all();
        if($cartItems->count()) {
            $price = $cartItems->sum(function($cart) {
                return $cart['discount_percent'] == 0
                    ? $cart['product']->price * $cart['quantity']
                    : ($cart['product']->price - ($cart['product']->price * $cart['discount_percent'])) * $cart['quantity'];
            });

            //در مپ ویت کی کلش تغییر میکنه
            $orderItems = $cartItems->mapWithKeys(function($cart) {
                return [$cart['product']->id => [ 'quantity' => $cart['quantity']] ];
            });

            $order = auth()->user()->orders()->create([
                'status' => 'unpaid',
                'price' => $price,
            ]);

            $order->products()->attach($orderItems);

            $token = config('services.payping.token');
            $res_number = Str::random();

            $args = [
                "amount" => 1000,
                "payerName" => auth()->user()->name,
                "returnUrl" => route('payment.callback'),
                "clientRefId" => $res_number
            ];

            $payment = new \PayPing\Payment($token);

            try {
                $payment->pay($args);
            } catch (\Exception $e) {
                throw $e;
            }
            //echo $payment->getPayUrl();
            $order->payments()->create([
                'resnumber' => $res_number,
                'price' => $price
            ]);

            $cartItems->flush();

            return redirect($payment->getPayUrl());
        }
        // alert()->error();
        return back();
    }

    public function callback(Request $request)
    {
        $payment = Payment::where('resnumber', $request->clientrefid)->firstOrFail();

        $token = config('services.payping.token');

        $payping = new \PayPing\Payment($token);

        try {
            // $payment->price
            if($payping->verify($request->refid, 1000)){
                $payment->update([
                    'status' => 1
                ]);

                $payment->order()->update([
                    'status' => 'paid'
                ]);

                alert()->success('پرداخت شما موفق بود');
                return redirect('/products');
            }else{
                alert()->error('پرداخت شما تایید نشد');
                return redirect('/profile/orders');
            }
        } catch (\Exception $e) {
            $errors = collect(json_decode($e->getMessage() , true));

            alert()->error($errors->first());
            return redirect('/products');
        }
    }
}
