<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query();

        if($keyword = request('search')) {
            $products->where('title' , 'LIKE' , "%{$keyword}%")->orWhere('user_id' , 'LIKE' , "%{$keyword}%" );
        }



        $products = $products->latest()->paginate(20);

        return view('admin.products.all',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required'],
            'price' => ['required'],
            'inventory' => ['required'],
            'categories' => ['required'],
            'attributes' => ['array'],
            'image' => ['required'],
        ]);



        $product = auth()->user()->products()->create($data);
        $product->categories()->sync($data['categories']);

        $this->attachAttributesToProduct($product, $data);


        alert()->success('محصول مورد نظر شما با موفقیت ایجاد شد');

        return redirect(route('admin.products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required'],
            'price' => ['required'],
            'inventory' => ['required'],
//            'image' => ['required'],
            'categories' => ['required'],
            'attributes' => ['array'],
        ]);


//        if ($request->file('image')){
//            $request->validate([
//                'image' => ['required','mimes:jpeg,png,jpg,bmp','max:2048'],
//            ]);
//
//            if (File::exists(public_path($product->image))){
//                File::delete(public_path($product->image));
//
//                $file = $request->file('image');
//                $destinationPath = '/images/product' . '/';
//                $file->move(public_path($destinationPath),$file->getClientOriginalName());
//
//                $data['image'] = $destinationPath . $file->getClientOriginalName();
//            }
//        }

        // method disk('public')
        Storage::putFileAs('files',$request->file('file') , $request->file('file')->getClientOriginalName());

        $product->update($data);
        $product->categories()->sync($data['categories']);

        $product->attributes()->detach();
        $this->attachAttributesToProduct($product, $data);

        alert()->success('محصول مورد نظر شما با موفقیت تغییر یافت');

        return redirect(route('admin.products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        File::delete(public_path($product->image));
        alert()->success('محصول مورد نظر شما با موفقیت حذف شد');
        return back();
    }

    /**
     * @param Product $product
     * @param array $data
     */
    protected function attachAttributesToProduct(Product $product, array $data): void
    {
        $attributes = collect($data['attributes']);
        $attributes->each(function ($item) use ($product) {
            if (is_null($item['name'] || $item['value'])) return false;

            $attr = Attribute::firstOrcreate([
                'name' => $item['name']
            ]);

            $attr_value = $attr->values()->firstOrCreate([
                'value' => $item['value']
            ]);

            $product->attributes()->attach($attr->id, ['value_id' => $attr_value['id']]);
        });
    }
}
