<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
//        $product = Product::search('موبایل')->select(['inventory'])->where('price' , 30000)->with('categories')->orderBy('price' , 'asc')->paginateRaw(10);
//        return $product;
        auth()->loginUsingId(1);
        $this
            ->seo()
            ->setTitle('Blogxer.ir')
            ->setDescription('بلاگکسر جایی است که همه می توانند بنویسند در بلاگسر میتوانید مطالبتان را انتشار دهید.
            بلاگسر جایی است برای خواندن و نوشتن');
        return view('index');
    }
}
