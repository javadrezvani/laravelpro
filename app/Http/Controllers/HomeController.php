<?php

namespace App\Http\Controllers;

use App\Services\FooService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth' , 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function comment(Request $request)
    {
//        if (! $request->ajax()){
//            return response()->json([
//                'status' => 'just ajax request'
//            ]);
//        }

        $data = $request->validate([
            'commentable_id' => 'required',
            'commentable_type' => 'required',
            'parent_id' => 'required',
            'comment' => 'required|min:10|max:255',
        ],[
            'comment.min' => 'نظر شما نباید کمتر از 10 کاراکتر داشته باشد',
            'comment.max' => 'نظر شما نباید بیشتر از 255 کاراکتر داشته باشد'
        ]);

        auth()->user()->comments()->create($data);

        alert()->success('  و پس از تایید در وب سایت نمایش داده خواهد شد','با تشکر ! دیدگاه شما با موفقیت ثبت شد')->persistent('بسیار خب');

        return back();
    }
}
