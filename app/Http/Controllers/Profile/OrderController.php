<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    //

    public function index()
    {
        $orders = auth()->user()->orders()->latest('created_at')->paginate(12);
        return view('profile.order-list' , compact('orders'));
    }

    public function showDetails(Order $order)
    {
        $this->authorize('view' , $order);
        return view('profile.order-detail',compact('order'));
    }

    public function payment(Order $order)
    {
        $this->authorize('view' , $order);
        $token = config('services.payping.token');
        $res_number = Str::random();
        $args = [
            "amount" => 1000 ,
            "payerName" => auth()->user()->name,
            "returnUrl" => route('payment.callback'),
            "clientRefId" => $res_number
        ];

        $payment = new \PayPing\Payment($token);

        try {
            $payment->pay($args);
        } catch (\Exception $e) {
            throw $e;
        }
        //echo $payment->getPayUrl();
        $order->payments()->create([
            'resnumber' => $res_number,
            'price' => $price
        ]);

        return redirect($payment->getPayUrl());
    }
}
