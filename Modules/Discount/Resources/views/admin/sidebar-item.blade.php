<li class="nav-item">
    <a href="{{ route('admin.discount.index') }}" class="nav-link {{ isActive('admin.discount.index') }}">
        <i class="nav-icon fa fa-dashboard"></i>
        <p>مدیریت تخفیف ها</p>
    </a>
</li>
