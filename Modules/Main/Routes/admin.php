<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::get('modules' , 'ModuleController@index')->name('modules.index');
Route::patch('modules/{module}/disable' , 'ModuleController@disable')->name('modules.disable');
Route::patch('modules/{module}/enable' , 'ModuleController@enable')->name('modules.enable');


