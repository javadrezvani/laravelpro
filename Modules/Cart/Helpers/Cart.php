<?php


namespace Modules\Cart\Helpers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Collection;

/**
 * Class Cart
 * @package App\Helpers\Cart
 * @method static bool has($id);
 * @method static \Illuminate\Support\Collection all();
 * @method static array get($id);
 * @method static Cart put(array $value , Model $obj = null);
 * @method static Cart update($key , $options );
 * @method static Cart instance(string $name);
 * @method static Cart count($key);
 * @method static Cart getDiscount();
 *
 */
class Cart extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cart';
    }
}
