<?php

namespace Modules\Cart\Http\Controllers\FrontEnd;


use App\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cart\Helpers\Cart;

class CartController extends Controller
{
    public function cart()
    {

        return view('cart::frontend.cart');
    }

    public function addToCart(Product $product)
    {
        if(Cart::has($product)) {
            if(Cart::count($product) < $product->inventory)
                Cart::update($product , 1);
        }else {
            Cart::put(
                [
                    'quantity' => 1,
                ],
                $product
            );
        }

        return redirect('/cart');
    }

    public function quantityChange(Request $request)
    {
        $data = $request->validate([
            'quantity' => 'required',
            'id' => 'required',
//           'cart' => 'required'
        ]);

        if( Cart::has($data['id']) ) {
            Cart::update($data['id'] , [
                'quantity' => $data['quantity']
            ]);

            return response(['status' => 'success']);
        }

        return response(['status' => 'error'] , 404);
    }

    public function deleteFromCart($id)
    {
        Cart::delete($id);

        return back();
    }
}
