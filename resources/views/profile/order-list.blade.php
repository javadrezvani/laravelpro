@extends('profile.layout')


@section('main')
    <table class="table">
        <tbody>
        <tr>
            <th>شماره سفارش</th>
            <th>تاریخ ثبت</th>
            <th>وضعیت سفارش</th>
            <th>کد رهگیری</th>
            <th>اقدامات</th>
        </tr>

        @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ jdate($order->created_at)->format('%d %B %Y') }}</td>

                @switch($order->status)
                    @case('paid')
                       <td class="badge badge-success">پرداخت شده</td>
                    @break
                        @case('unpaid')
                        <td class="badge badge-warning">در انتظار پرداخت</td>
                    @break
                @endswitch
                <td>{{ $order->tracking_serial ?? 'در انتظار ثبت سفارش' }}</td>
                <td>
                    <a href="{{ route('profile.orders.detail' , $order->id) }}" class="btn btn-sm btn-info">جزئیات سفارش</a>
                    @if($order->status == 'unpaid')
                        <a href="{{ route('profile.orders.payment',$order->id) }}" class="btn btn-sm btn-warning">پرداخت سفارش</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $orders->render() }}
@endsection
