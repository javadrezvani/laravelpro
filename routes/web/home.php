<?php


use App\User;

use \Illuminate\Support\Facades\Gate;
use \Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','indexController@index')->name('index');
Route::get('/test',function (){
    dd();
   $module = Module::find('Discount');
   $module->enable();
});

//Route::get('/a', function () {
//    auth()->loginUsingId(1);
////    return route('download.file' , ['user' => auth()->user()->id() , 'path' => 'files/1.jpg']);
//
////    return URL::temporarySignedRoute('download.file' , now()->addMinutes(10) , ['user' => auth()->user()->id , 'path' => 'files/angel-origgi-yZO8H4OqKHY-unsplash.jpg']);
//
//    //return now();
//
//    //$product = \App\Product::find(10);
//
////    $product->comments()->create([
////        'user_id' => auth()->user()->getAuthIdentifier(),
////        'comment' => 'this is my second comment',
////    ]);
////
////    return $product->comments()->get();
//
////   foreach ($product->attributes as $attribute){
////       return $attribute->pivot->value;
////   }
//
//    //$comment = \App\Comment::find(1);
//
//    return view('welcome');
//});

Auth::routes(['verify' => true]);
Route::get('/auth/google' ,'Auth\GoogleAuthController@redirect')->name('auth.google');
Route::get('/auth/google/callback' ,'Auth\GoogleAuthController@callback');
Route::prefix('auth')->namespace('Auth')->group(function (){
    Route::get('/github' ,'GithubAuthController@redirect')->name('auth.github');
    Route::get('/github/callback' ,'GithubAuthController@callback');
});
Route::get('/auth/token' ,'Auth\AuthTokenController@getToken')->name('2fa.token');
Route::post('/auth/token' ,'Auth\AuthTokenController@postToken');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/secret' , function() {
    return 'secret';
})->middleware(['auth' , 'password.confirm']);

Route::get('download/{user}/file',function ($file){
    return Storage::download(request('path'));
})->middleware('signed')->name('download.file');

Route::prefix('profile')->namespace('Profile')->middleware(['auth' , 'verified'])->group(function() {
    Route::get('/' , 'IndexController@index')->name('profile');
    Route::get('twofactor' , 'TwoFactorAuthController@manageTwoFactor')->name('profile.2fa.manage');
    Route::post('twofactor' , 'TwoFactorAuthController@postManageTwoFactor');
    Route::get('twofacto/phone' , 'TokenAuthController@getPhoneVerify')->name('profile.2fa.phone');
    Route::post('twofacto/phone' , 'TokenAuthController@postPhoneVerify');
});

Route::middleware('auth')->group(function() {
    Route::prefix('profile')->namespace('Profile')->group(function() {
        Route::get('/' , 'IndexController@index')->name('profile');
        Route::get('twofactor' , 'TwoFactorAuthController@manageTwoFactor')->name('profile.2fa.manage');
        Route::post('twofactor' , 'TwoFactorAuthController@postManageTwoFactor');
        Route::get('twofacto/phone' , 'TokenAuthController@getPhoneVerify')->name('profile.2fa.phone');
        Route::post('twofacto/phone' , 'TokenAuthController@postPhoneVerify');
        Route::get('orders' , 'OrderController@index')->name('profile.orders');
        Route::get('order/{order}','OrderController@showDetails')->name('profile.orders.detail');
        Route::get('order/{order}/payment','OrderController@payment')->name('profile.orders.payment');
    });
    Route::post('comments' , 'HomeController@comment')->name('send.comment');
    Route::post('payment','PaymentController@payment')->name('cart.payment');
    Route::get('payment/callback','PaymentController@callback')->name('payment.callback');
});

Route::get('products','ProductController@index');
Route::get('products/{product}','ProductController@single');

